<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Logica</title>
</head>
<body>
    <h1>Ejemplo de operaciones logicas en PHP</h1>
    <?php
         $a = 8;
         $b = 3;
         $c = 3;
         echo ($a == $b) && ($C > $b),"<br>";
         echo ($a == $b) || ($b == $c),"<br>";
         //este tira error :'v
        // echo !($b <= $c)$b,"<br>";
        //asi no tira
        echo !($b <= $c),"<br>";
         //error
         //pregunta                respuesta
         // &&                     Y
         // ||                     O

         // 7 ejercicio 
          $i = 9;
          $f = 33.5;
          $c = 'X';

         if(($i>=6) && ($c=='X'))
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if(($i>=6) || ($c==12))
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if(($f<11) && ($i>100))
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if(($c !='P') || (($i+$f) <= 10))
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if(($i + $f)<= 10)
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if((i >= 6) && ($c =='X'))
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }
         if(($c != 'P') || ($i +$f)<=10)
         {
             echo ("true<br>");
         }else
         {
            echo ("false<br>");
         }

    ?>
</body>
</html>
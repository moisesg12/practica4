<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Delimitadores</title>
    <link rel="stylesheet" href="css/delimeters.css">
    <link rel="stylesheet" href="css/tabs.css">
  </head>
  <body>
    <header>
      <h1>Los delimitadores de codigo PHP</h1><br>
    </header>
    <section>
      <article>
        <div class="contenedor-tabs">
            <?php
            echo "<span class=\"diana\" id=\"una\"></span>\n";
            echo "<div class=\"tab\">\n";
             echo "<a href=\"#una\" class=\"tab-e\">Estilo XML</a>\n";
             echo "<div class=\"first\">n";
            echo "<p class=\"xmltag\">\natcasesort(array)";
            echo "Este es un texto que esta escrito en php, Utilizando estas etiquetas mas";
            echo "usuales y recomendadas para delimitar el codigo en php, que son: ";
            echo "&lt;?php ... ?&gt;. <br>\n";
            echo "</p>\n";
            echo "</div>\n";
            echo "</div>\n";
            ?>
            <?php
            echo"<span class=\"diana\" id=\"tres\"></span>\n";
            echo "<div class=\"tab\">\n";
            echo "<a href=\"#tres\" class=\"tab-e\">etiquetas cortas</a>";
            echo "<div>\n";
            echo "<p class=\"shorttag\">";
            echo "este un mensaje escrito por php, utilizando etiquetas";
            echo "cortas, <br>\n que son: &lt;? ...  ?&gt;";
            echo "</p>\n";
            echo "</div>\n";
            echo "</div>\n";
            ?>
    </div>
      </article>
    </section>
  </body>
</html>

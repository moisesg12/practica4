<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comparacion</title>
</head>
<body>
    <h1>Ejemplo de operadores de Comparacion en PHP</h1>
    <?php
        $a = 8;
        $b = 3;
        $c = 3;
        echo $a == $b,"<br>";
        echo $a != $b,"<br>";
        echo $a < $b,"<br>";
        echo $a > $b,"<br>";
        echo $a >= $c,"<br>";
        echo $a <= $c,"<br>";
        //pregunta                respuesta
        // ==                     operador de igualdad
        // !=                     diferente de
        // <                      menor que
        // >                      mayor que
        // >=                     mayot o igual que
        // <=                     menor o igual que
    ?>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>tipos y valores</title>
</head>
<body>
    <?php
        $un_bool = true;
        $un_str= "programacion";
        $un_str2= 'programacion';
        $un_int= 12;

        echo gettype($un_bool);
        echo "<br>";
        echo gettype($un_str);
        echo "<br>";
        if(is_int($un_int))
        {
            $un_int += 4;
            echo ($un_int);
            echo "<br>";
        }
        if(is_string($un_bool))
        {
            echo "cadena: $un_bool";
        }
    ?>
</body>
</html>
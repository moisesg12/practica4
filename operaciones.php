<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Operaciones</title>
</head>
<body>
    <h1>Ejemplos de operaciones aritmeticas en PHP</h1>
    <?php
        $a = 8;
        $b = 3;
        echo $a + $b, "<br>";
        echo $a - $b, "<br>";
        echo $a * $b, "<br>";
        echo $a / $b, "<br>";
        $a++;
        echo $a, "<br>";
        $b--;
        echo $b, "<br>";
    ?>
</body>
</html>